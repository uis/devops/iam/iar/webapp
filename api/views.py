"""
Views implementing the API endpoints.

"""
from collections import namedtuple

from automationcommon.models import set_local_user, clear_local_user
from django.conf import settings
from django.db.models import Q, Count
from django.utils.decorators import method_decorator
from django.utils.timezone import now
from django_filters.rest_framework import (
    DjangoFilterBackend, FilterSet, CharFilter, BooleanFilter, ChoiceFilter
)
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, decorators, status, mixins
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.permissions import DjangoModelPermissions
from rest_framework.response import Response
from django.http import HttpResponse
import csv

from . import lookup
from iar.models import Asset
from .permissions import (
    OrPermission, AndPermission,
    UserInInstitutionPermission, UserInIARGroupPermission
)
from .serializers import (
    AssetSerializer, AssetStatsSerializer, ProfileSerializer, AssetDumpSerializer
)


#: Decorator to apply to DRF methods which sets the appropriate security requirements.
SCHEMA_DECORATOR = swagger_auto_schema(security=[{'token': []}])


class AssetFilter(FilterSet):
    """
    A custom DjangoFilterBackend filter_class defining all filterable fields.
    It seems that simply using filter_fields doesn't work with the is_complete fields.

    This class was implemented using the example here:
    https://django-filter.readthedocs.io/en/latest/guide/rest_framework.html#adding-a-filterset-with-filter-class
    """
    department = CharFilter()
    purpose = ChoiceFilter(choices=Asset.PURPOSE_CHOICES)
    owner = CharFilter()
    private = BooleanFilter()
    personal_data = BooleanFilter()
    recipients_outside_uni = ChoiceFilter(choices=Asset.RECIPIENTS_OUTSIDE_CHOICES)
    recipients_outside_eea = ChoiceFilter(choices=Asset.RECIPIENTS_OUTSIDE_CHOICES)
    retention = ChoiceFilter(choices=Asset.RETENTION_CHOICES)
    is_complete = BooleanFilter()
    # TODO:
    # It seem's probable that we would like to filter on the following MultiSelectField fields.
    # However, we should implement this when we know how we would like to filter them.
    #   data_subject,
    #   data_category,
    #   risk_type,
    #   storage_format,
    #   paper_storage_security,
    #   digital_storage_security

    class Meta:
        model = Asset
        fields = [
            'department', 'purpose', 'owner', 'private', 'personal_data', 'recipients_outside_eea',
            'recipients_outside_uni', 'is_complete', 'retention'
        ]


@method_decorator(name='create', decorator=SCHEMA_DECORATOR)
@method_decorator(name='retrieve', decorator=SCHEMA_DECORATOR)
@method_decorator(name='update', decorator=SCHEMA_DECORATOR)
@method_decorator(name='partial_update', decorator=SCHEMA_DECORATOR)
@method_decorator(name='destroy', decorator=SCHEMA_DECORATOR)
@method_decorator(name='list', decorator=SCHEMA_DECORATOR)
class AssetViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows assets to be created, viewed, searched, filtered, and ordered
    by any field.

    To order by a specific field you need to include in your GET request a parameter called
    ordering with the name of the field you want to order by. You can also order in reverse
    by adding the character "-" at the beginning of the name of the field.

    You can also use the parameter search in your request with the text that you want to search.
    This text will be searched on all fields and will return all possible results

    You can also filter by a specific field. For example if you only want to return those assets
    with name "foobar" you can add to your GET request a parameter called name (name of the field)
    and the value you want to filter by. Example ?name=foobar (this will return all assets
    that have as name "foobar").

    """
    queryset = Asset.objects.filter(deleted_at__isnull=True)
    serializer_class = AssetSerializer

    ordering = ('-created_at',)
    filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter)
    filterset_class = AssetFilter
    search_fields = (
        'name', 'purpose_other',
        'recipients_outside_uni_description',
        'recipients_outside_eea_description',
        'risk_type_additional', 'storage_location',
        # TODO
        # a search on the following fields won't work as the user would probably expect as they
        # store a mnemonic and not the text that the user sees.
        'department', 'purpose', 'owner', 'data_subject', 'data_category', 'retention',
        'risk_type', 'storage_format', 'paper_storage_security', 'digital_storage_security',
    )
    ordering_fields = search_fields + (
        'id', 'private', 'personal_data', 'recipients_outside_uni', 'recipients_outside_eea',
        'created_at', 'updated_at', 'is_complete'
    )

    permission_classes = [
        OrPermission(
            DjangoModelPermissions, AndPermission(
                UserInIARGroupPermission, UserInInstitutionPermission
            )
        ),
    ]

    def initial(self, request, *args, **kwargs):
        """Runs anything that needs to occur prior to calling the method handler."""
        # Perform any authentication, permissions checking etc.
        super().initial(request, *args, **kwargs)

        # Set the local user for the auditing framework
        set_local_user(request.user)

    def finalize_response(self, request, response, *args, **kwargs):
        """
        Returns the final response object.
        """
        # By this time the local user has been recorded if necessary.
        clear_local_user()

        return super().finalize_response(request, response, *args, **kwargs)

    def get_queryset(self):
        """
        get_queryset is patched to only return those assets that are not private or that are
        private but the user doing the request belongs to department that owns the asset.

        Also, if the user is not in :py:attr:`~iar.defaultsettings.IAR_USERS_LOOKUP_GROUP`,
        they can't see assets.
        """
        if self.request.user.is_anonymous:
            return Asset.objects.none()

        user_group_names = lookup.get_group_names_for_email(self.request.user.email)
        if settings.IAR_USERS_LOOKUP_GROUP not in user_group_names:
            return Asset.objects.none()

        queryset = super(AssetViewSet, self).get_queryset()

        institutions = lookup.get_instids_for_email(self.request.user.email)

        return queryset.filter(Q(private=False) | Q(private=True, department__in=institutions))

    def update(self, request, *args, **kwargs):
        """We force a refresh after an update, so we can get the up to date annotation data."""
        super(AssetViewSet, self).update(request, *args, **kwargs)

        return Response(self.get_serializer(self.get_object()).data)

    def perform_destroy(self, instance):
        """perform_destroy patched to not delete the instance but instead flagged as deleted."""
        if instance.deleted_at is None:
            instance.deleted_at = now()
            instance.save()


AssetCounts = namedtuple('AssetCounts', 'total completed with_personal_data')


class AssetStats:
    """Given a queryset of matching non-annotated asset records, calculate some statistics for all
    assets and assets grouped by department.
    We need the non-annotated queryset to work around a bug in Django. See
    https://code.djangoproject.com/ticket/28762 and
    https://github.com/uisautomation/iar-backend/issues/55.

    :param queryset: Non-annotated queryset of :py:class:`iar.models.Asset` objects

    :ivar AssetCounts all: Counts for all assets
    :ivar dict by_institution: An :py:class:`AssetCounts` instance for each institution keyed by
        Lookup instid.

    """

    def __init__(self, queryset):
        # Annotate the input queryset with is_complete.
        annotated_queryset = Asset.objects.annotate_is_complete(queryset)

        # Compute asset entry counts for all assets.
        self.all = AssetCounts(
            total=queryset.count(),
            # This works around bug that throws an exception
            completed=queryset.filter(
                id__in=annotated_queryset.filter(is_complete=True).values('id')).count(),
            with_personal_data=queryset.filter(personal_data=True).count()
        )

        # Compute individual counts grouped by department for each class of entry.
        n_assets_by_dept = {
            d['department']: d['count'] for d in
            queryset.values('department').annotate(count=Count('id')).order_by('department')
        }
        n_assets_completed_by_dept = {
            d['department']: d['count'] for d in
            annotated_queryset.filter(is_complete=True).values('department')
            .annotate(count=Count('id')).order_by('department')
        }
        n_assets_with_personal_data_by_dept = {
            d['department']: d['count'] for d in
            queryset.filter(personal_data=True)
            .values('department').annotate(count=Count('id')).order_by('department')
        }

        # Create a list of all departments in the database
        all_depts = [
            row['department'] for row in
            queryset.distinct('department').values('department').order_by('department')
        ]

        # Collect separate counts together grouped by institution. If there is no count for a
        # particular institution, report "0".
        self.by_institution = {
            dept: AssetCounts(
                total=n_assets_by_dept.get(dept, 0),
                completed=n_assets_completed_by_dept.get(dept, 0),
                with_personal_data=n_assets_with_personal_data_by_dept.get(dept, 0),
            )
            for dept in all_depts
        }


class StatsViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
    Returns Assets stats: total number of assets, total number of assets completed,
    total number of assets with personal data, and assets per department (total, completed,
    with personal data)
    """
    queryset = Asset.objects.none()
    serializer_class = AssetStatsSerializer
    permission_classes = []
    pagination_class = None

    @swagger_auto_schema(responses={status.HTTP_200_OK: AssetStatsSerializer}, security=[])
    def list(self, request):
        # These statistics should only be for non-deleted assets.
        serializer = self.serializer_class(
            AssetStats(Asset.objects.get_base_queryset().filter(deleted_at__isnull=True)))
        return Response(data=serializer.data)


class ProfileViewSet(viewsets.ViewSet):
    @swagger_auto_schema(responses={status.HTTP_200_OK: ProfileSerializer})
    @decorators.action(detail=False, methods=['get'])
    def user(self, request):
        """
        Obtain basic profile information for the current authenticated user.

        """
        if request.user.is_anonymous:
            serializer = ProfileSerializer({'is_anonymous': True})
        else:
            user_group_names = lookup.get_group_names_for_email(request.user.email)
            serializer = ProfileSerializer({
                'is_anonymous': False,
                'email': request.user.email,
                'display_name': self._get_user_display_name(request.user),
                'groups': user_group_names,
                'institutions': lookup.get_instids_for_email(request.user.email),
                'is_authorised': settings.IAR_USERS_LOOKUP_GROUP in user_group_names
            })
        return Response(data=serializer.data)

    def _get_user_display_name(self, user):
        """Return a human-friendly display name for a user."""
        # An anonymous user has no display name
        if user.is_anonymous:
            return ''

        # Form display name by concatenating first and last names and stripping whitespace
        display_name = user.get_full_name().strip()

        # If the display name is empty, use the username instead
        if display_name == '':
            return user.username

        return display_name


class DumpViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = Asset.objects.filter(deleted_at__isnull=True)

    pagination_class = None
    serializer_class = AssetDumpSerializer

    ordering = ('-created_at',)
    filter_backends = (DjangoFilterBackend, )
    filterset_fields = ('department', )

    permission_classes = [
        OrPermission(
            DjangoModelPermissions, AndPermission(
                UserInIARGroupPermission, UserInInstitutionPermission
            )
        ),
    ]

    def get_queryset(self):
        """
        get_queryset is patched to only return those assets that are not private or that are
        private but the user doing the request belongs to department that owns the asset.

        Also, if the user is not in :py:attr:`~iar.defaultsettings.IAR_USERS_LOOKUP_GROUP`,
        they can't see assets.
        """
        if self.request.user.is_anonymous:
            return Asset.objects.none()

        user_group_names = lookup.get_group_names_for_email(self.request.user.email)
        if settings.IAR_USERS_LOOKUP_GROUP not in user_group_names:
            return Asset.objects.none()

        institutions = lookup.get_instids_for_email(self.request.user.email)

        queryset = self.filter_queryset(self.queryset)

        return queryset.filter(
            Q(private=False) | Q(private=True, department__in=institutions)
        )

    def list(self, request):
        department = request.query_params.get('department', 'all')
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = f'attachment; filename="assets-{department}.csv"'
        serializer = self.serializer_class(
            self.get_queryset(), many=True,
            context={
                'request': request,
                'allInsts': lookup.get_all_institutions()  # pass all institutions to serializer
            }
        )

        # Format header changing "some_value" to "Some Value" plus some special case
        # all caps for Url and Eea
        def formatted_header(header):
            return {
                k: k.replace('_', ' ').title().replace('Url', 'URL').replace('Eea', 'EEA')
                for k in header
            }

        if len(serializer.data) > 0:
            header = serializer.data[0].keys()
            writer = csv.DictWriter(response, fieldnames=header)
            # Customised header row
            writer.writerow(formatted_header(header))
            writer.writerows(serializer.data)

        return response
