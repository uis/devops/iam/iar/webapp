import copy
import csv
from io import StringIO

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse
from django.utils.timezone import now
from rest_framework.test import APIClient
from iar.models import Asset
from api.serializers import AssetSerializer
from iar.tests.test_models import COMPLETE_ASSET as UNDERSCORED_COMPLETE_ASSET
from automationcommon.models import set_local_user
from djangorestframework_camel_case.util import camelize

from . import TestCase


# API responses will be in camelCase
COMPLETE_ASSET = camelize(UNDERSCORED_COMPLETE_ASSET)


def merge_dicts(*dicts, deleted_keys=[]):
    """Return a new dict from a list of dicts by repeated calls to update(). Keys in later
    dicts will override those in earlier ones. *deleted_keys* is a list of keys which should be
    deleted from the resulting dict.

    """
    rv = {}
    for d in dicts:
        rv.update(d)
    for k in deleted_keys:
        del rv[k]
    return rv


class APIViewsTests(TestCase):
    def setUp(self):
        super().setUp()
        self.maxDiff = None

        # Patch lookup functions
        self.addCleanup(self.patch_lookup())

    def _get(self, data=None, format='json'):
        return self.client.get(
            reverse('api:asset-list'), data=data, format=format)

    def _post(self, data, format='json'):
        return self.client.post(reverse('api:asset-list'), data, format=format)

    def _patch(self, pk, data, format='json'):
        return self.client.patch(
            reverse('api:asset-detail', kwargs={'pk': pk}), data, format=format)

    def _put(self, pk, data, format='json'):
        return self.client.patch(
            reverse('api:asset-detail', kwargs={'pk': pk}), data, format=format)

    def _get_asset(self, pk=None, url=None, format='json'):
        if url is None:
            self.assertIsNotNone(pk)
            url = reverse('api:asset-detail', kwargs={'pk': pk})
        return self.client.get(url, format=format)

    def test_asset_post_auth(self):
        # Create unauthenticated client
        self.create_client()
        response = self._post(COMPLETE_ASSET)
        self.assert_unauthorised(response)

    def test_asset_post_validation(self):
        """User's only allow to post an asset that has a department their are part of"""
        asset_dict = copy.copy(COMPLETE_ASSET)
        asset_dict['department'] = 'TESTDEPT2'
        result_post = self._post(asset_dict)
        self.assertEqual(result_post.status_code, 403)

    def test_asset_post_id(self):
        """POST-ing a new asset gives it an id."""
        result_post = self._post(COMPLETE_ASSET)
        self.assertEqual(result_post.status_code, 201)
        result_get = self._get_asset(url=result_post.data['url'])
        self.assertEqual(result_get.status_code, 200)
        result_get_dict = result_get.data
        self.assertIn('id', result_get_dict)
        self.assertIsNotNone(result_get_dict['id'])

    def test_search_filter(self):
        # Test that the search fields finds an asset called asset2 out of some assets
        asset_dict1 = copy.copy(COMPLETE_ASSET)
        asset_dict2 = copy.copy(COMPLETE_ASSET)
        asset_dict3 = copy.copy(COMPLETE_ASSET)
        asset_dict1['name'] = "asset1"
        asset_dict2['name'] = "asset2"
        asset_dict3['name'] = "asset3"
        self.assertEqual(self._post(asset_dict1).status_code, 201)
        self.assertEqual(self._post(asset_dict2).status_code, 201)
        self.assertEqual(self._post(asset_dict3).status_code, 201)
        result_get = self._get({'search': "asset2"})
        result_get_dict = result_get.data
        self.assertTrue("results" in result_get_dict)
        self.assertEqual(len(result_get_dict["results"]), 1)
        expected_dict = copy.copy(UNDERSCORED_COMPLETE_ASSET)
        expected_dict['name'] = "asset2"
        self.assert_dict_list_equal(expected_dict, result_get_dict["results"][0],
                                    ignore_keys=('created_at', 'updated_at', 'url', 'is_complete',
                                                 'id', 'allowed_methods'))

    def test_order_filter(self):
        # test that we can order the list of all assets by name (asc, desc)
        asset_dict1 = copy.copy(COMPLETE_ASSET)
        asset_dict2 = copy.copy(COMPLETE_ASSET)
        asset_dict3 = copy.copy(COMPLETE_ASSET)
        asset_dict1['name'] = "asset1"
        asset_dict2['name'] = "asset2"
        asset_dict3['name'] = "asset3"
        self.assertEqual(self._post(asset_dict2).status_code, 201)
        self.assertEqual(self._post(asset_dict3).status_code, 201)
        self.assertEqual(self._post(asset_dict1).status_code, 201)
        result_get = self._get({'ordering': "name"})
        result_get_dict = result_get.data
        self.assertTrue("results" in result_get_dict)
        self.assertEqual(len(result_get_dict["results"]), 3)
        self.assertEqual(result_get_dict["results"][0]["name"], "asset1")
        self.assertEqual(result_get_dict["results"][1]["name"], "asset2")
        self.assertEqual(result_get_dict["results"][2]["name"], "asset3")

        result_get = self._get({'ordering': "-name"})
        result_get_dict = result_get.data
        self.assertTrue("results" in result_get_dict)
        self.assertEqual(len(result_get_dict["results"]), 3)
        self.assertEqual(result_get_dict["results"][0]["name"], "asset3")
        self.assertEqual(result_get_dict["results"][1]["name"], "asset2")
        self.assertEqual(result_get_dict["results"][2]["name"], "asset1")

    def test_is_complete_on_put(self):
        """Test that is_complete is refreshed on an update"""
        asset_dict1 = copy.copy(COMPLETE_ASSET)
        del asset_dict1['name']
        result_post = self._post(asset_dict1)
        self.assertEqual(result_post.status_code, 201)
        result_get = self._get_asset(url=result_post.data['url'])
        self.assertEqual(result_get.status_code, 200)
        self.assertFalse(result_get.data["is_complete"])
        result_put = self.client.put(result_post.data['url'], COMPLETE_ASSET, format='json')
        self.assertEqual(result_put.status_code, 200)
        self.assertTrue(result_put.data["is_complete"])

    def test_is_complete_on_patch(self):
        """Test that is_complete is refreshed on an update"""
        asset_dict1 = copy.copy(COMPLETE_ASSET)
        del asset_dict1['name']
        result_post = self._post(asset_dict1)
        self.assertEqual(result_post.status_code, 201)
        result_get = self._get_asset(url=result_post.data['url'], format='json')
        self.assertEqual(result_get.status_code, 200)
        self.assertFalse(result_get.data["is_complete"])
        result_patch = self.client.patch(
            result_post.data['url'], {"name": "asset1"}, format='json')
        self.assertEqual(result_patch.status_code, 200)
        self.assertTrue(result_patch.data["is_complete"])

    def test_asset_patch_validation(self):
        """User's only allow to PATCH an asset that has a department their are part of, and the
        PATCH department has to be one he belongs to"""
        asset_dict = copy.copy(UNDERSCORED_COMPLETE_ASSET)
        asset_dict['department'] = 'TESTDEPT2'
        asset = Asset(**asset_dict)
        asset.save()
        result_patch = self._patch(asset.pk, {"name": "asset1"})
        # Not allowed because the asset belongs to TESTDEPT2
        self.assert_method_is_not_listed_as_allowed('PATCH', asset)
        self.assertEqual(result_patch.status_code, 403)

        # We fix the department, so now the user should be allow but we try to change the
        # department to another that the user doesn't belong to
        asset.department = 'TESTDEPT'
        set_local_user(self.user)
        asset.save()

        # User is in principle allowed ...
        self.assert_method_is_listed_as_allowed('PATCH', asset)

        # ... but not in this case
        result_patch = self._patch(asset.pk, {"department": "TESTDEPT2"})
        self.assertEqual(result_patch.status_code, 403)

        # This one should be allowed
        result_patch = self._patch(asset.pk, {"name": "asset2"})
        self.assert_method_is_listed_as_allowed('PATCH', asset)
        self.assertEqual(result_patch.status_code, 200)

    def test_asset_put_validation(self):
        """User's only allow to PUT an asset that has a department their are part of, and the
        PUT department has to be one he belongs to"""
        asset_dict = copy.copy(UNDERSCORED_COMPLETE_ASSET)
        asset_dict['department'] = 'TESTDEPT2'
        asset = Asset(**asset_dict)
        asset.save()
        result_put = self._put(asset.pk, COMPLETE_ASSET)
        # Not allowed because the asset belongs to TESTDEPT2
        self.assert_method_is_not_listed_as_allowed('PUT', asset)
        self.assertEqual(result_put.status_code, 403)

        # We fix the department, so now the user should be allow but we try to change the
        # department to another that the user doesn't belong to
        asset.department = 'TESTDEPT'
        set_local_user(self.user)
        asset.save()

        # User can, in principle PUT...
        self.assert_method_is_listed_as_allowed('PUT', asset)

        # ... but not this asset
        result_put = self._put(asset.pk, asset_dict)
        self.assertEqual(result_put.status_code, 403)

        # This one should be allowed
        asset_dict['department'] = 'TESTDEPT'
        asset_dict['name'] = 'asset2'
        result_put = self._put(asset.pk, asset_dict)
        self.assert_method_is_listed_as_allowed('PUT', asset)
        self.assertEqual(result_put.status_code, 200)

    def test_privacy(self):
        """Test that a User cannot see/access to assets that are private outside their
        department"""
        asset_dict = copy.copy(UNDERSCORED_COMPLETE_ASSET)
        asset_dict['department'] = 'TESTDEPT2'
        asset_dict['private'] = True
        asset = Asset(**asset_dict)
        asset.save()
        result_get = self._get_asset(pk=asset.pk)
        self.assertEqual(result_get.status_code, 404)
        list_assets = self._get()
        self.assertEqual(list_assets.data['results'], [])

    def test_delete(self):
        """Test that the asset is not deleted but marked as deleted"""
        asset_dict1 = copy.copy(COMPLETE_ASSET)
        result_post = self._post(asset_dict1)
        self.assertEqual(result_post.status_code, 201)
        asset = Asset.objects.get(pk=result_post.data['id'])
        self.assertIsNone(asset.deleted_at)

        # Check that there is 1 asset listed
        list_assets = self._get()
        self.assertNotEqual(list_assets.data['results'], [])

        # Change user institution
        old_instids = self.user_instids
        self.user_instids = ['SOMEOTHERDEPT']
        result_delete = self.client.delete(result_post.data['url'])
        # User's institution doesn't match asset institution
        self.assert_method_is_not_listed_as_allowed('DELETE', asset)
        self.assertEqual(result_delete.status_code, 403)

        self.user_instids = old_instids
        self.assert_method_is_listed_as_allowed('DELETE', asset)

        result_delete = self.client.delete(result_post.data['url'])
        # User's institution match asset institution
        self.assertEqual(result_delete.status_code, 204)
        asset.refresh_from_db()
        self.assertIsNotNone(asset.deleted_at)

        # Check that no assets are listed
        list_assets = self._get()
        self.assertEqual(list_assets.data['results'], [])

        # Check that you can't retrieve an asset
        asset_get = self._get_asset(url=result_post.data['url'])
        self.assertEqual(asset_get.status_code, 404)

    def test_delete_with_perms(self):
        """Super users can delete any asset,"""
        asset_dict1 = copy.copy(COMPLETE_ASSET)
        result_post = self._post(asset_dict1)
        self.assertEqual(result_post.status_code, 201)
        asset = Asset.objects.get(pk=result_post.data['id'])
        self.assertIsNone(asset.deleted_at)

        # Change user institution
        self.user_instids = ['SOMEOTHERDEPT']

        # DELETE not in allowed methods
        self.assert_method_is_not_listed_as_allowed('DELETE', asset)

        # Initially fails
        result_delete = self.client.delete(result_post.data['url'])
        self.assertEqual(result_delete.status_code, 403)

        # Succeeds if use has permission
        perm = Permission.objects.get(
            content_type=ContentType.objects.get_for_model(Asset), codename='delete_asset')
        self.user.user_permissions.add(perm)
        self.user.save()

        self.refresh_user()

        self.assertTrue(self.user.has_perm('iar.delete_asset'))

        # DELETE is now in allowed methods
        self.assert_method_is_listed_as_allowed('DELETE', asset)

        result_delete = self.client.delete(result_post.data['url'])
        self.assertEqual(result_delete.status_code, 204)

    def test_patch_with_perms(self):
        """A user with the change asset permission can patch an asset."""
        asset_dict = copy.copy(UNDERSCORED_COMPLETE_ASSET)
        asset_dict['department'] = 'TESTDEPT2'
        asset = Asset(**asset_dict)
        asset.save()

        # PATCH not in allowed methods
        self.assert_method_is_not_listed_as_allowed('PATCH', asset)

        result_patch = self._patch(asset.pk, {"name": "asset1"})

        # Not allowed because the asset belongs to TESTDEPT2
        self.assertEqual(result_patch.status_code, 403)

        # Succeeds if use has permission
        perm = Permission.objects.get(
            content_type=ContentType.objects.get_for_model(Asset), codename='change_asset')
        self.user.user_permissions.add(perm)
        self.user.save()

        self.refresh_user()

        self.assertTrue(self.user.has_perm('iar.change_asset'))

        # PATCH is now in allowed methods
        self.assert_method_is_listed_as_allowed('PATCH', asset)

        result_patch = self._patch(asset.pk, {"name": "asset1"})
        self.assertEqual(result_patch.status_code, 200)

    def test_put_with_perms(self):
        """A user with the change asset permission can put an asset."""
        asset_dict = copy.copy(UNDERSCORED_COMPLETE_ASSET)
        asset_dict['department'] = 'TESTDEPT2'
        asset = Asset(**asset_dict)
        asset.save()

        # PUT not in allowed methods
        self.assert_method_is_not_listed_as_allowed('PUT', asset)

        result_put = self._put(asset.pk, COMPLETE_ASSET)

        # Not allowed because the asset belongs to TESTDEPT2
        self.assertEqual(result_put.status_code, 403)

        # Succeeds if use has permission
        perm = Permission.objects.get(
            content_type=ContentType.objects.get_for_model(Asset), codename='change_asset')
        self.user.user_permissions.add(perm)
        self.user.save()

        self.refresh_user()

        self.assertTrue(self.user.has_perm('iar.change_asset'))

        # PUT is now in allowed methods
        self.assert_method_is_listed_as_allowed('PUT', asset)

        result_put = self._put(asset.pk, COMPLETE_ASSET)
        self.assertEqual(result_put.status_code, 200)

    def test_post_with_perms(self):
        """A user with the create asset permission can post an asset."""
        asset_dict = copy.copy(COMPLETE_ASSET)
        asset_dict['department'] = 'TESTDEPT2'
        result_post = self._post(asset_dict)
        self.assertEqual(result_post.status_code, 403)

        # Succeeds if use has permission
        perm = Permission.objects.get(
            content_type=ContentType.objects.get_for_model(Asset), codename='add_asset')
        self.user.user_permissions.add(perm)
        self.user.save()

        self.refresh_user()

        self.assertTrue(self.user.has_perm('iar.add_asset'))
        result_post = self._post(asset_dict)
        self.assertEqual(result_post.status_code, 201)

    def test_iar_users_group_membership(self):
        """check that the user can do/see nothing if they aren't in uis-iar-users"""
        # remove group membership
        self.user_group_names = []

        # create a asset
        asset = Asset.objects.create(**UNDERSCORED_COMPLETE_ASSET)

        # test no assets are listed
        self.assertEqual(self._get().data['results'], [])

        # test single asset isn't visible
        self.assertEqual(self._get_asset(pk=asset.pk).status_code, 404)

        # test all change operations fail with 403
        self.assertEqual(self._post(COMPLETE_ASSET).status_code, 403)
        self.assertEqual(self._put(asset.pk, COMPLETE_ASSET).status_code, 403)
        self.assertEqual(self._patch(asset.pk, {'name': 'new'}, format='json').status_code, 403)
        self.assertEqual(self.client.delete(
            reverse('api:asset-detail', kwargs={'pk': asset.pk})).status_code, 403)

    def test_asset_stats(self):
        """The asset stats endpoint reports correct statistics."""
        set_local_user(self.user)  # Some user which will be used in the audit log

        # Retrieve from empty database stats
        response = self.client.get(reverse('api:stats-list'), format='json')
        self.assertEqual(response.status_code, 200)

        self.assertDictEqual(response.data, {
            'all': {'total': 0, 'completed': 0, 'with_personal_data': 0},
            'by_institution': {},
        })

        # A complete asset
        self.create_asset_from_dict(UNDERSCORED_COMPLETE_ASSET)

        # An incomplete asset w/ no personal data
        self.create_asset_from_dict(merge_dicts(
            UNDERSCORED_COMPLETE_ASSET, {'personal_data': False}, deleted_keys=['name']
        ))

        # An asset from TESTDEPT2
        self.create_asset_from_dict(merge_dicts(
            UNDERSCORED_COMPLETE_ASSET, {'department': 'TESTDEPT2'}
        ))

        # A deleted asset
        asset = self.create_asset_from_dict(UNDERSCORED_COMPLETE_ASSET)
        asset.deleted_at = now()
        asset.save()

        # Retrieve the stats
        response = self.client.get(reverse('api:stats-list'), format='json')
        self.assertEqual(response.status_code, 200)

        self.assertDictEqual(response.data, {
            'all': {'total': 3, 'completed': 2, 'with_personal_data': 2},
            'by_institution': {
                'TESTDEPT': {'total': 2, 'completed': 1, 'with_personal_data': 1},
                'TESTDEPT2': {'total': 1, 'completed': 1, 'with_personal_data': 1},
            },
        })

    def refresh_user(self, reauthenticate=True):
        """Refresh user from the database."""
        self.user = get_user_model().objects.get(pk=self.user.pk)
        if reauthenticate:
            self.create_client(user=self.user)

    def assert_unauthorised(self, response):
        """Assert that a response has a status of 401 Unauthorized."""
        self.assertEqual(response.status_code, 401)

    def assert_dict_list_equal(self, odict1, odict2, ignore_keys=(), msg=None):
        """
        :param odict1: dictionary 1 that you want to compare to dictionary 2
        :param odict2: dictionary 2 that you want to compare to dictionary 1
        :param ignore_keys: list of the dictionary keys you don't want to compare
        :param msg: pass statement - shown on failure
        :type odict1: dict
        :type odict2: dict
        :type ignore_keys: list
        Compares two dictionary with lists (ignoring the order of the lists).
        """
        d1 = copy.copy(odict1)
        d2 = copy.copy(odict2)
        for k in ignore_keys:
            d1.pop(k, None)
            d2.pop(k, None)
        for k, v in d1.items():
            if v.__class__ == list:
                d1[k] = set(v)
        for k, v in d2.items():
            if v.__class__ == list:
                d2[k] = set(v)
        return self.assertDictEqual(d1, d2, msg)

    def assert_method_is_listed_as_allowed(self, method, asset):
        """Assert that a given method appears in the allowed_methods list for an asset."""
        result_get = self._get_asset(pk=asset.pk)
        self.assertEqual(result_get.status_code, 200)
        self.assertIn(method, result_get.data['allowed_methods'])

    def assert_method_is_not_listed_as_allowed(self, method, asset):
        """Assert that a given method does not appear in the allowed_methods list for an asset."""
        result_get = self._get_asset(pk=asset.pk)
        self.assertEqual(result_get.status_code, 200)
        self.assertNotIn(method, result_get.data['allowed_methods'])

    def create_asset_from_dict(self, asset_dict):
        """Return an iar.models.Asset object from a dictionary of the form accepted by the POST
        endpoint. The object has also been save()-ed to the database so the pk attribute will be
        valid. The dictionary is assert-ed to be valid via the is_valid() method on the
        AssetSerializer.

        """
        asset_serializer = AssetSerializer(data=asset_dict)
        self.assertTrue(asset_serializer.is_valid())
        return Asset.objects.create(**asset_serializer.validated_data)


# An alternate asset to COMPLETE_ASSET. It is intended the this asset is never filtered for in
# AssetFilterTests.
DIFFERENT_ASSET = camelize({
    "name": "asset2",
    "department": "OTHER",
    "purpose": "other",
    "purpose_other": "Something else",
    "owner": None,
    "private": True,
    "personal_data": False,
    "data_subject": [],
    "data_category": [],
    "recipients_outside_uni": None,
    "recipients_outside_uni_description": None,
    "recipients_outside_eea": None,
    "recipients_outside_eea_description": None,
    "retention": None,
    "risk_type": [
        "operational", "reputational"
    ],
    "risk_type_additional": None,
    "storage_location": "Who knows",
    "storage_format": [
        "digital", "paper"
    ],
    "paper_storage_security": [
        "locked_cabinet"
    ],
    "digital_storage_security": [
        "acl"
    ],
})


class AssetFilterTests(TestCase):
    """
    Tests relating to the custom DjangoFilterBackend filter_class -> AssetFilter
    """
    def setUp(self):
        super().setUp()
        self.maxDiff = None

        # Patch lookup functions
        self.addCleanup(self.patch_lookup())

        # Add two assets for testing
        self._post(COMPLETE_ASSET)
        self._post(DIFFERENT_ASSET)

    def _get(self, data, format='json'):
        return self.client.get(reverse('api:asset-list'), data=data, format=format)

    def _post(self, data, format='json'):
        return self.client.post(reverse('api:asset-list'), data, format=format)

    def test_filter_by_department(self):
        self.assertAsset1(self._get({'department': 'TESTDEPT'}))

    def test_filter_by_purpose(self):
        self.assertAsset1(self._get({'purpose': 'research'}))

    def test_filter_by_owner(self):
        self.assertAsset1(self._get({'owner': 'amc203'}))

    def test_filter_by_private(self):
        self.assertAsset1(self._get({'private': 'false'}))

    def test_filter_by_personal_data(self):
        self.assertAsset1(self._get({'personalData': 'true'}))

    def test_filter_by_recipients_outside_uni(self):
        self.assertAsset1(self._get({'recipientsOutsideUni': 'yes'}))

    def test_filter_by_recipients_outside_eea(self):
        self.assertAsset1(self._get({'recipientsOutsideEea': 'no'}))

    def test_filter_by_retention(self):
        self.assertAsset1(self._get({'retention': '<1'}))

    def test_filter_is_complete(self):
        self.assertAsset1(self._get({'isComplete': 'true'}))

    def assertAsset1(self, results):
        """
        Assert that only the result set contains exactly the COMPLETE_ASSET.
        :param results: test Assets result set.
        """
        result_get_dict = results.data
        self.assertTrue("results" in result_get_dict)
        self.assertEqual(len(result_get_dict['results']), 1)
        self.assertEqual(result_get_dict['results'][0]['name'], 'asset1')


class SwaggerAPITest(TestCase):
    """
    Tests relating to the use of Swagger (OpenAPI)
    """
    def test_security_definitions(self):
        """API spec should define an oauth2 security requirement."""
        spec = self.get_spec()
        self.assertIn('securityDefinitions', spec)
        self.assertIn('oauth2', spec['securityDefinitions'])

    def get_spec(self):
        """Return the Swagger (OpenAPI) spec as parsed JSON."""
        client = APIClient()
        response = client.get(reverse('schema-json', kwargs={'format': '.json'}))
        self.assertEqual(response.status_code, 200)
        return response.data


class ProfileTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.maxDiff = None

        # Patch lookup functions
        self.addCleanup(self.patch_lookup())

    def test_authenticated(self):
        """
        Basic use of authenticated (and authorised) profile succeeds.

        """
        response = self.client.get(reverse('api:profile-user'), format='json')
        self.assertEqual(response.status_code, 200)  # OK
        self.assertEqual(response.data['is_anonymous'], False)
        self.assertEqual(response.data['email'], self.user.email)
        self.assertEqual(response.data['display_name'], self.user.get_full_name())
        self.assertListEqual(response.data['institutions'], self.user_instids)
        self.assertListEqual(response.data['groups'], self.user_group_names)
        self.assertEqual(response.data['is_authorised'], True)

    def test_authenticated_but_not_authorised(self):
        """
        Basic use of authenticated (but not authorised) profile succeeds but not authorised.

        """
        self.user_group_names.remove(settings.IAR_USERS_LOOKUP_GROUP)
        response = self.client.get(reverse('api:profile-user'), format='json')
        self.assertEqual(response.status_code, 200)  # OK
        self.assertEqual(response.data['is_anonymous'], False)
        self.assertEqual(response.data['is_authorised'], False)

    def test_unauthenticated(self):
        """
        Basic use of unauthenticated profile get succeeds.

        """
        self.create_client()
        response = self.client.get(reverse('api:profile-user'), format='json')
        self.assertEqual(response.status_code, 200)  # OK
        self.assertEqual(response.data['is_anonymous'], True)


class DumpTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.maxDiff = None

        # Patch lookup functions
        self.addCleanup(self.patch_lookup())

        # Add example assets
        #   asset1 : TESTDEPT : not private
        #   asset2 : TESTDEPT : private
        #   asset3 : TESTDEPT2 : not private
        #   asset4 : TESTDEPT2 : private
        asset_dict = copy.copy(UNDERSCORED_COMPLETE_ASSET)
        asset_dict['name'] = 'asset1'
        asset_dict['department'] = 'TESTDEPT'
        asset = Asset(**asset_dict)
        asset.save()
        asset_dict['name'] = 'asset2'
        asset_dict['private'] = True
        asset = Asset(**asset_dict)
        asset.save()
        asset_dict['name'] = 'asset4'
        asset_dict['department'] = 'TESTDEPT2'
        asset = Asset(**asset_dict)
        asset.save()
        asset_dict['name'] = 'asset3'
        asset_dict['private'] = False
        asset = Asset(**asset_dict)
        asset.save()

    def getCSVRows(self, data=None):
        response = self.client.get(
            reverse('api:dump-list'), data=data
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'text/csv')
        # response content contains multiple rows separated by newlines
        reader = csv.reader(
            StringIO(response.content.decode('utf-8')),
            lineterminator='\n',
            delimiter=',',
            quotechar='"'
        )
        return [row for row in reader]

    def test_unauthenticated(self):
        """
        Basic use of unauthenticated dump should fail

        """
        self.create_client()
        response = self.client.get(reverse('api:dump-list'))
        self.assertEqual(response.status_code, 401)

    def test_csv_response(self):
        """
        Basic use of authenticated dump should return a CSV file

        """
        rows = self.getCSVRows()
        self.assertGreater(len(rows), 1)
        # at least the header row contains multiple fields separated by commas
        self.assertGreater(len(rows[0]), 1)
        # header contains at least 'id' and 'name'
        self.assertIn('Id', rows[0])
        self.assertIn('Name', rows[0])

    def test_privacy(self):
        """
        Test that a User can see all assets of their department and only non-private
        assets from another department

        """
        rows = self.getCSVRows()
        # strip header
        self.assertGreater(len(rows), 1)
        rows = rows[1:]

        # should leave 2 from TESTDEPT and 1 (non-private) from TESTDEPT2
        self.assertEqual(len(rows), 3)
        # should have rows containing assets 1, 2 and 3 but not 4
        self.assertTrue(self.a_row_contains(rows, 'asset1'))
        self.assertTrue(self.a_row_contains(rows, 'asset2'))
        self.assertTrue(self.a_row_contains(rows, 'asset3'))
        self.assertFalse(self.a_row_contains(rows, 'asset4'))

    def test_filter_own_department(self):
        """
        Test that a User can filter to a department they are a member of all get a limited result

        """
        rows = self.getCSVRows(data={'department': 'TESTDEPT'})
        # strip header
        self.assertGreater(len(rows), 1)
        rows = rows[1:]

        # should leave just 2 from TESTDEPT
        self.assertEqual(len(rows), 2)
        # should have rows containing assets 1, 2 but not 3 nor 4
        self.assertTrue(self.a_row_contains(rows, 'asset1'))
        self.assertTrue(self.a_row_contains(rows, 'asset2'))
        self.assertFalse(self.a_row_contains(rows, 'asset3'))
        self.assertFalse(self.a_row_contains(rows, 'asset4'))

    def test_filter_other_department(self):
        """
        Test that a User can filter to a department they aren't a member of and get only
        non-private results

        """
        rows = self.getCSVRows(data={'department': 'TESTDEPT2'})
        # strip header
        self.assertGreater(len(rows), 1)
        rows = rows[1:]
        # should leave just 1 from TESTDEPT2
        self.assertEqual(len(rows), 1)
        # should have just a row containing assets 3
        self.assertFalse(self.a_row_contains(rows, 'asset1'))
        self.assertFalse(self.a_row_contains(rows, 'asset2'))
        self.assertTrue(self.a_row_contains(rows, 'asset3'))
        self.assertFalse(self.a_row_contains(rows, 'asset4'))

    def a_row_contains(self, rows, required_value):
        for row in rows:
            for value in row:
                if value == required_value:
                    return True
        return False
