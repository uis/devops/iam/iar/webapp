from unittest import mock

from django.conf import settings
from django.contrib.auth import get_user_model
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import RefreshToken


class TestCase(APITestCase):
    """
    Django TestCase subclass with some useful utility methods.

    """
    def setUp(self):
        super().setUp()

        # Create a test user and client authorised as that user.
        self.user = get_user_model().objects.create_user(
            username='test0001', email='test0001@cam.ac.uk',
            first_name='Test', last_name='User')
        self.auth_client = self.create_client(user=self.user)

        # Default values for patch_lookup()
        self.user_instids = ['TESTDEPT']
        self.user_group_names = [settings.IAR_USERS_LOOKUP_GROUP]

    def patch_lookup(self, *, email=None, instids=None, group_names=None):
        """
        Patch lookup utility functions so that the passed user email returns the passed instids and
        group names. The defaults for these being the values of self.user_instids and
        self.user_group_names. If the passed email is None, the email of self.user is user.

        Returns a callable which should be called to remove the patch.

        """
        target_user_email = email if email is not None else self.user.email

        def get_instids():
            return instids if instids is not None else self.user_instids

        def get_group_names():
            return group_names if group_names is not None else self.user_group_names

        get_instids_for_email_patcher = mock.patch(
            'api.lookup.get_instids_for_email',
            side_effect=lambda email: get_instids() if email == target_user_email else []
        )
        get_group_names_for_email_patcher = mock.patch(
            'api.lookup.get_group_names_for_email',
            side_effect=lambda email: get_group_names() if email == target_user_email else []
        )
        get_all_institutions_patcher = mock.patch(
            'api.lookup.get_all_institutions',
            return_value=[
                ('TESTDEPT', 'First Department'),
                ('TESTDEPT2', 'Second Department'),
            ]
        )

        def stop():
            get_instids_for_email_patcher.stop()
            get_group_names_for_email_patcher.stop()
            get_all_institutions_patcher.stop()

        get_instids_for_email_patcher.start()
        get_group_names_for_email_patcher.start()
        get_all_institutions_patcher.start()

        return stop

    def create_client(self, *, user=None):
        """
        Update APITestCase.client authenticated to act as the passed user.
        If the passed user is None or anonymous, the client is unauthenticated.

        """
        if user is not None and not user.is_anonymous:
            assert user.email != ''
            self.client.force_authenticate(user=user)
            token = RefreshToken.for_user(user)
            self.client.credentials(HTTP_AUTHORIZATION=f'Token {token.access_token}')
        else:
            self.client.force_authenticate()
            self.client.credentials()
        return self.client
