"""
Utility functions to interact with lookup.

Because we support multiple authentication mechanisms via django-social-auth we cannot rely on the
username of a user to be the crsid. Indeed we should not rely on this since the django-social-auth
backends are free to make usernames unique by appending random characters. Instead we use the
email address as a unique identifier. This in turn means that our permissions model relies on the
email address associated with a user being verified. As such, do not use an authentication backend
which auto-creates users without verifying the email address.

Email addresses of the form [a-z0-9]{3,8}@cam\\.ac\\.uk are assumed to have crsids as the local
part. As such the functions in this file will only return meaningful results for email addresses of
that form.

"""
import functools
import logging
import re

from ucamlookup.utils import get_connection, get_institutions
from ucamlookup.ibisclient import PersonMethods, IbisException

LOG = logging.getLogger(__name__)

#: A pattern used to match a crsid containing email address and to extract the crsid.
CRSID_EMAIL_PATTERN = re.compile(r'^(?P<crsid>[a-z0-9]{3,8})@cam\.ac\.uk$')


@functools.lru_cache()
def get_instids_for_email(email):
    """
    Return a list of instids given a user's email address. If the user cannot be found or if the
    email address is not of the form [crsid]@cam.ac.uk, an empty list is returned.

    The return value is cached based on the email address so it is safe to call this multiple
    times.

    """
    crsid = _crsid_for_email(email)
    if crsid is None:
        return []

    person_methods = PersonMethods(get_connection())
    try:
        return [inst.instid for inst in person_methods.getInsts('crsid', crsid)]
    except IbisException as e:
        LOG.warning('Error fetching info for "%s"', email)
        LOG.warning('%r', e)
    return []


@functools.lru_cache()
def get_group_names_for_email(email):
    """
    Return a list of group names given a user's email address. If the user cannot be found or if
    the email address is not of the form [crsid]@cam.ac.uk, an empty list is returned.

    The return value is cached based on the email address so it is safe to call this multiple
    times.

    """
    crsid = _crsid_for_email(email)
    if crsid is None:
        return []

    person_methods = PersonMethods(get_connection())
    try:
        return [group.name for group in person_methods.getGroups('crsid', crsid)]
    except IbisException as e:
        LOG.warning('Error fetching info for "%s"', email)
        LOG.warning('%r', e)
    return []


def _crsid_for_email(email):
    """
    Return the crsid associated with the passed email address or None if there is none.

    """
    crsid_match = CRSID_EMAIL_PATTERN.match(email)
    if not crsid_match:
        return None
    return crsid_match.group('crsid')


def get_all_institutions():
    return get_institutions()
