"""
URL routing schema for API

"""

from rest_framework import routers

from . import views

app_name = 'api'

router = routers.DefaultRouter()
router.register('assets', views.AssetViewSet, basename='asset')
router.register('stats', views.StatsViewSet, basename='stats')
router.register('profiles', views.ProfileViewSet, basename='profile')
router.register('dump', views.DumpViewSet, basename='dump')

urlpatterns = router.urls
