"""
The :py:mod:`iar_project.settings_testsuite` module contains settings which are
specific to the test suite environment. The default ``tox`` test environment uses this settings
module when running the test suite.

"""
import copy
import json
import os

# Import settings from the base settings file
from .base import *  # noqa: F401, F403

DEBUG = True

# In GitLab CI, the tox test runner still sets the old-style DJANGO_DB_... environment variables.
# As such support them but only when running tests.
_db_envvar_prefix = 'DJANGO_DB_'
for name, value in os.environ.items():
    # Only look at variables which start with the prefix we expect
    if not name.startswith(_db_envvar_prefix):
        continue

    # Remove prefix
    name = name[len(_db_envvar_prefix):]

    # Set value
    DATABASES['default'][name] = value  # noqa: F405

#: The default test runner is changed to one which captures stdout and stderr
#: when running tests.
TEST_RUNNER = 'iar_project.test.runner.BufferedDiscoverRunner'

#: Static files are collected into a directory determined by the tox
#: configuration. See the tox.ini file.
STATIC_ROOT = os.environ.get('TOX_STATIC_ROOT')

# When running under tox, it is useful to see the database config. Make a deep copy and censor the
# password.
_db_copy = copy.deepcopy(DATABASES)  # noqa: F405
for v in _db_copy.values():
    if 'PASSWORD' in v:
        v['PASSWORD'] = '<redacted>'
print('Databases:')
print(json.dumps(_db_copy, indent=2))
