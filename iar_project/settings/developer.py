# Import settings from the base settings file
from .base import *  # noqa: F401, F403

from datetime import timedelta

DEBUG = True


def _show_toolbar(request):
    from django.conf import settings
    return settings.DEBUG


# Configure the django debug toolbar if it is installed.
try:
    import debug_toolbar  # noqa: F401

    INSTALLED_APPS = INSTALLED_APPS + [  # noqa: F405
        'debug_toolbar',
    ]

    MIDDLEWARE = MIDDLEWARE + [  # noqa: F405
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    ]

    DEBUG_TOOLBAR_CONFIG = {
        # Bypass the INTERNAL_IPS check since, within the development docker container, we don't
        # know what the host IP is likely to be.
        'SHOW_TOOLBAR_CALLBACK': _show_toolbar,
    }
except ImportError:
    pass

STATIC_URL = '/static/'

# Enable the browsable API renderer in development
REST_FRAMEWORK['DEFAULT_RENDERER_CLASSES'].extend([  # noqa: F405
    'rest_framework.renderers.BrowsableAPIRenderer',
])

SIMPLE_JWT.update({  # noqa: F405
    # In development drastically reduce access and refresh token lifetimes to surface any
    # refresh/sign out issues.
    'ACCESS_TOKEN_LIFETIME': timedelta(minutes=5),
    'REFRESH_TOKEN_LIFETIME': timedelta(hours=2),
})
