"""
Serialisers for API resources.

"""
from rest_framework import serializers


class RefreshTokenSerializer(serializers.Serializer):
    """
    A django-rest-framework-simplejwt RefreshToken.

    """
    refresh = serializers.CharField(source='*', required=False)
    access = serializers.CharField(source='access_token')


class SignInSettingsSerializer(serializers.Serializer):
    """
    Settings for user-sign in.

    """
    google_client_id = serializers.CharField(help_text='OAuth2 client id for Google sign in')
    google_scopes = serializers.ListField(
        child=serializers.CharField(),
        help_text='List of scopes required for Google sign in'
    )


class VerifyTokenSerializer(serializers.Serializer):
    """
    Response from verifying a token.

    """


class UnauthorizedSerializer(serializers.Serializer):
    """
    401 Unauthorized response when token is invalid.

    """
    detail = serializers.CharField()
    code = serializers.CharField()
