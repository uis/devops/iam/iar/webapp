"""
Views implementing the API endpoints for API token-based auth.

"""
import re

from django.conf import settings
from drf_yasg.utils import swagger_auto_schema
from rest_framework import (
    exceptions,
    response,
    status,
    views,
)
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import (
    TokenRefreshView as BaseTokenRefreshView,
    TokenVerifyView as BaseTokenVerifyView,
)
from social_django.utils import psa

from . import serializers


class SignInSettingsView(views.APIView):
    """
    A view which renders settings required for social sign in.

    """
    # This view is unauthenticated
    authentication_classes = []

    @swagger_auto_schema(
        security=[],
        responses={status.HTTP_200_OK: serializers.SignInSettingsSerializer},
        operation_id='tokens_settings_exchange',
    )
    def get(self, request):
        """
        Settings required for client-side sign in support.

        """
        serializer = serializers.SignInSettingsSerializer({
            'google_client_id': settings.SOCIAL_AUTH_GOOGLE_OAUTH2_KEY,
            'google_scopes': settings.SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE,
        })
        return response.Response(serializer.data)


class TokenExchangeView(views.APIView):
    """
    A view which knows how to exchange a social auth access token for an API access/refresh token
    pair.

    """
    # We essentially perform our own authentication directly via social_django so don't get DRF to
    # do any for us.
    authentication_classes = []

    @swagger_auto_schema(
        security=[{'oauth2': ['email', 'profile']}],
        responses={status.HTTP_201_CREATED: serializers.RefreshTokenSerializer},
        operation_id='tokens_exchange',
    )
    def post(self, request, *, backend):
        """
        Convert an access token from social sign in backend into an access token/refresh token
        pair. The backend should be one configured for the application, e.g. "google-oauth2", and
        the access token should be passed in the Authorization header in the format "Bearer
        {token}".

        """
        # Authenticate/create a user using the access token for the specified social backend. This
        # then gets converted into a simplejwt RefreshToken for the user.
        refresh_token = RefreshToken.for_user(self._complete_social_sign_in(request, backend))
        serializer = serializers.RefreshTokenSerializer(refresh_token)
        return response.Response(data=serializer.data, status=status.HTTP_201_CREATED)

    @staticmethod
    @psa('social:complete')
    def _complete_social_sign_in(request, backend):
        """
        Authenticate a user via a social authentication access token. The Authorization header
        should be set to "Bearer {access token}". Return the authenticated user or raise
        exceptions.AuthenticationFailed if the passed token was invalid or migging.

        This code is adapted from:
        https://python-social-auth.readthedocs.io/en/latest/use_cases.html#signup-by-oauth-access-token

        """
        # Extract the bearer token from the authorization header.
        authorization = request.headers.get('authorization')
        if authorization is None:
            raise exceptions.AuthenticationFailed('Missing authorization header')

        match = re.match('^Bearer (?P<token>.*)$', authorization)
        if not match:
            raise exceptions.AuthenticationFailed('Authorization header has wrong format')

        user = request.backend.do_auth(match.group('token'))
        if user is None or user.is_anonymous:
            raise exceptions.AuthenticationFailed('Authorised as anonymous user')

        return user


class TokenRefreshView(BaseTokenRefreshView):
    """
    Use a refresh token to obtain a new access token. If refresh token rotating is enabled, this
    view will return a new refresh token along with the access token.

    """
    # This view is unauthenticated
    authentication_classes = []

    @swagger_auto_schema(
        security=[],
        responses={
            status.HTTP_201_CREATED: serializers.RefreshTokenSerializer,
            status.HTTP_401_UNAUTHORIZED: serializers.UnauthorizedSerializer,
        },
        operation_id='tokens_refresh',
    )
    def post(self, *args, **kwargs):
        """
        Obtain a new access token from a refresh token. If refresh token rotation is enabled, a new
        refresh token is returned as well.

        """
        return super().post(*args, **kwargs)


class TokenVerifyView(BaseTokenVerifyView):
    """
    Verify an access token. This endpoint returns an empty object if verification succeed otherwise
    it returns a 401 Unauthorized response.

    """
    # This view is unauthenticated
    authentication_classes = []

    @swagger_auto_schema(
        security=[],
        responses={
            status.HTTP_200_OK: serializers.VerifyTokenSerializer,
            status.HTTP_401_UNAUTHORIZED: serializers.UnauthorizedSerializer,
        },
        operation_id='tokens_verify',
    )
    def post(self, *args, **kwargs):
        """
        Verify an access token. If the token is valid, the response is an empty object otherwise a
        401 Unauthorized response is returned.

        """
        return super().post(*args, **kwargs)
