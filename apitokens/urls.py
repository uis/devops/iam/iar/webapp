"""
URLs for API token auth

"""
from django.urls import path

from . import views

app_name = 'apitokens'

urlpatterns = [
    path('settings/exchange/', views.SignInSettingsView.as_view(), name='settings-sign-in'),
    path('exchange/<backend>/', views.TokenExchangeView.as_view(), name='token-exchange'),
    path('refresh/', views.TokenRefreshView.as_view(), name='token-refresh'),
    path('verify/', views.TokenVerifyView.as_view(), name='token-verify'),
]
