#!/usr/bin/env sh
#
# Wrapper script to run tox. Arguments are passed directly to tox.

# Exit on failure
set -e

# Change to this script's directory
cd "$(dirname "$0")"

# Execute tox runner, logging command used
set -x
exec ./compose.sh tox run --rm $COMPOSE_ARGS tox tox $@
