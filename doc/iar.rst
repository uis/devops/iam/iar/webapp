Information Asset Register
===============================================================================

Installation
````````````

Add the ``iar`` application to your
``INSTALLED_APPS`` configuration as usual.

Views and serializers
`````````````````````

.. automodule:: iar.views
    :members:

Default URL routing
```````````````````

.. automodule:: iar.urls
    :members:

Application configuration
`````````````````````````

.. automodule:: iar.apps
    :members:
