from django.apps import AppConfig


class Config(AppConfig):
    """Configuration for Information Asset Register application."""
    #: The short name for this application. This should be the Python module name since Django uses
    #: this to import things.
    name = 'iar'

    #: The human-readable verbose name for this application.
    verbose_name = 'Information Asset Register'
