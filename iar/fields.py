from multiselectfield.db.fields import MultiSelectField as _MultiSelectField


# Override for MultiSelectField from django-multiselectfield package to ensure compatibility
# with Django 4.2.0. This subclass addresses an indexing issue related to the handling of
# the `max_length` attribute by the original MultiSelectField in Django 4.2.0.
class MultiSelectField(_MultiSelectField):
    def __init__(self, *args, **kwargs):
        # Capture the original `max_length` value provided.
        original_max_length = kwargs.get('max_length', None)

        # Temporarily set `max_length` to 0 to bypass the indexing issue
        # during superclass initialization. This workaround prevents the
        # initialization process from encountering the incompatibility issue.
        kwargs['max_length'] = 0

        # Initialize the superclass with the modified `kwargs`.
        super().__init__(*args, **kwargs)

        # Restore the original `max_length` value after initialization.
        # This ensures the field behaves as expected in models and forms,
        # maintaining the intended functionality of specifying a maximum length.
        self.max_length = original_max_length
