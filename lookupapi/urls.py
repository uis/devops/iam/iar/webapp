"""
URLs for Lookup API (currently just Institutions)

"""
from django.urls import path

from . import views

app_name = 'lookupapi'

urlpatterns = [
    path('institutions/', views.InstitutionsListView.as_view(), name='institutions-list'),
    path('institutions/<instid>/', views.InstitutionsView.as_view(), name='institutions-detail'),
    path('people/', views.PeopleSearchView.as_view(), name='people-list'),
    path('people/<crsid>', views.PersonFindView.as_view(), name='people-detail'),
]
