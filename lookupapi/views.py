"""
Views implementing the API endpoints for lookup API

"""
from rest_framework import generics, permissions, response
from ucamlookup.utils import (
    get_institutions, get_institution_name_by_id, get_users_from_query, return_visibleName_by_crsid
)

from . import serializers


class InstitutionsListView(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = serializers.InstitutionListSerializer
    # Prevent pagination as we're returning a list not a real queryset
    pagination_class = None

    def get_queryset(self):
        # Empty queryset to remove swagger generation warning
        return []

    def list(self, request):
        insts = [{'instid': a[0], 'name': a[1]} for a in get_institutions()]
        return response.Response(self.serializer_class(
            {
                'results': insts
            }, context={'request': request}).data)


class InstitutionsView(generics.RetrieveAPIView):
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = serializers.InstitutionSummarySerializer

    def get_object(self):
        instid = self.kwargs.get('instid')
        return {
            'instid': instid,
            'name': get_institution_name_by_id(instid)
        }


class PeopleSearchView(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = serializers.PeopleListSerializer
    # Prevent pagination as we're returning a list not a real queryset
    pagination_class = None

    def list(self, request):
        people = []
        query = request.query_params.get('query')
        if query is not None:
            limit = int(request.query_params.get('limit', 10))
            if limit > 0:
                people = get_users_from_query(query)[:limit]
        return response.Response(self.serializer_class(
            {
                'results': people
            }, context={'request': request}).data)


class PersonFindView(generics.RetrieveAPIView):
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = serializers.PersonSerializer

    def get_object(self):
        crsid = self.kwargs.get('crsid')
        return {
            'crsid': crsid,
            'visibleName': return_visibleName_by_crsid(crsid)
        }
