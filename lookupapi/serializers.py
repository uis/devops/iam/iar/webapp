"""
Serialisers for API resources.

"""
from rest_framework import serializers


class InstitutionSummarySerializer(serializers.Serializer):
    """
    Summary serializer for Institution.
    """
    instid = serializers.CharField(help_text='The institution\'s unique ID.')
    name = serializers.CharField(help_text='The institution\'s name.')


class InstitutionListSerializer(serializers.Serializer):
    """
    Institution list serializer.
    """
    results = InstitutionSummarySerializer(many=True)


class PersonSerializer(serializers.Serializer):
    """
    Individual person serializer
    """
    id = serializers.CharField(source='crsid', help_text='Person\'s unique ID (crsid).')
    name = serializers.CharField(source='visibleName', help_text='Person\'s visible name.')


class PeopleListSerializer(serializers.Serializer):
    """
    List of people serializer
    """
    results = PersonSerializer(many=True)
