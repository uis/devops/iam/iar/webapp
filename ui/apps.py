from django.apps import AppConfig


class Config(AppConfig):
    name = 'ui'
    verbose_name = 'Information Asset Register UI'
