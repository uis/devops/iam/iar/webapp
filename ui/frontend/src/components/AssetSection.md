### Examples

```jsx
import { Paper } from "@material-ui/core";
import CheckCircleOutlineSharpIcon from "@material-ui/icons/CheckCircleOutlineSharp";

const indicator = <CheckCircleOutlineSharpIcon style={{ color: "green" }} />;

<Paper>
  <AssetSection title="Section Title Here">
    <div>Section with no indicator</div>
  </AssetSection>

  <AssetSection title="Section Title Here" indicator={indicator}>
    <div>Section with indicator</div>
  </AssetSection>
</Paper>;
```
