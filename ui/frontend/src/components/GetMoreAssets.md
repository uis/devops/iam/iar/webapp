### Examples

```jsx
import { Button } from "@material-ui/core";

const [isLoading, setIsLoading] = React.useState(false);
const [nextPageTriggers, setNextPageTriggers] = React.useState(0);

const nextPage = () => {
  setNextPageTriggers(nextPageTriggers + 1);
  setIsLoading(true);
};

<>
  <div>Next page triggered {nextPageTriggers} times</div>
  <Button
    variant="contained"
    onClick={() => {
      setIsLoading(false);
    }}
  >
    {isLoading ? "Stop Loading" : "Not Loading"}
  </Button>
  <div style={{ height: "150px", paddingTop: "50px", textAlign: "center" }}>
    ...when the loading indicator below is visible, the next page will be
    triggered...
  </div>
  <GetMoreAssets isLoading={isLoading} onNextPage={nextPage} />
</>;
```
