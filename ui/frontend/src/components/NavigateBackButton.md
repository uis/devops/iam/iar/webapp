### Examples

```js
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Toolbar from "@material-ui/core/Toolbar";

<BrowserRouter>
  <Switch>
    <Route path="/">
      <Toolbar>
        <NavigateBackButton />
      </Toolbar>
    </Route>
  </Switch>
</BrowserRouter>;
```
