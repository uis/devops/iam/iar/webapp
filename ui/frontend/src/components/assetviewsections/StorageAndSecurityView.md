### Examples

```jsx
const asset = {
  storageLocation: "A secure location",
  storageFormat: ["paper", "digital"],
  digitalStorageSecurity: ["acl", "backup"],
  paperStorageSecurity: ["locked_building", "locked_room", "locked_cabinet"],
};

<StorageAndSecurityView asset={asset} />;
```
