### Examples

```jsx
const asset = {
  name: "Asset Name Here",
  department: "instid",
  purpose: "staff_administration",
};

<GeneralInformationView asset={asset} />;
```
