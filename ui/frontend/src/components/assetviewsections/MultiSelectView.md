### Examples

```jsx
import { Grid } from "@material-ui/core";

const options = [
  { value: "alpha", label: "Alpha option" },
  { value: "beta", label: "Beta option" },
  { value: "gamma", label: "Gamma option" },
  { value: "delta", label: "Delta option" },
];

const values = [options[0].value, options[2].value, options[3].value];

<Grid container spacing={4}>
  <MultiSelectView
    label="No options selected"
    values={null}
    options={options}
  />
  <MultiSelectView
    label="Single option selected"
    values={values.slice(0, 1)}
    options={options}
  />
  <MultiSelectView
    label="Multiple options selected"
    values={values}
    options={options}
  />
</Grid>;
```
