### Examples

```jsx
import { Grid } from "@material-ui/core";

<Grid container>
  <ReadOnlyLabelValue label="Label for data here" value="Value of data here" />
</Grid>;
```
