### Examples

```jsx
const asset = {
  personalData: true,
  dataSubject: ["students"],
  dataCategory: ["education", "social"],
  recipientsOutsideUni: "yes",
  recipientsOutsideUniDescription:
    "Description of recipients outside University",
  recipientsOutsideEea: "not_sure",
  retention: ">10,<=75",
};

<PersonalDataView asset={asset} />;
```
