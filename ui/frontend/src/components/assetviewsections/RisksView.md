### Examples

```jsx
const asset = {
  riskType: ["financial", "safety", "compliance"],
  riskTypeAdditional: "Additional risk information...",
};

<RisksView asset={asset} />;
```
