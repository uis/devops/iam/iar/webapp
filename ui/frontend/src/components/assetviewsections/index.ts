export { default as GeneralInformationView } from "./GeneralInformationView";
export { default as PersonalDataView } from "./PersonalDataView";
export { default as RisksView } from "./RisksView";
export { default as StorageAndSecurityView } from "./StorageAndSecurityView";
