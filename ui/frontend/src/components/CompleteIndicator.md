### Examples

```js
import { Checkbox } from "@material-ui/core";

const [isComplete, setIsComplete] = React.useState(false);

<div>
  <Checkbox
    checked={isComplete}
    value="complete"
    onChange={() => {
      setIsComplete(!isComplete);
    }}
  />
  Complete
  <h3 style={{ display: "flex" }}>
    <div style={{ flex: 1 }}>Section with indicator</div>
    <CompleteIndicator isComplete={isComplete} />
  </h3>
</div>;
```
