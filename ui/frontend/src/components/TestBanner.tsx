import * as React from "react";

import { Box, Button, Typography } from "@material-ui/core";

import { useAuthenticationState } from "../api";

/**
 * Test authenticated user with sign out of the site.
 */
interface TestBannerProps {
  url?: string;
  page?: string;
}

export const TestBanner: React.FunctionComponent<TestBannerProps> = ({
  url = "Unknown",
  page = "Unknown",
  children,
}) => {
  const { profile, signOut } = useAuthenticationState();

  return (
    <Box>
      <Box p={2} mx="auto" textAlign="center">
        <Box pb={2}>
          <Typography variant="h4" component="p" gutterBottom>
            {page}
          </Typography>
          <Typography variant="body1" component="p" gutterBottom>
            ({url})
          </Typography>
        </Box>
        <Box pb={2}>
          <Typography variant="body1" component="p" gutterBottom>
            You are signed in as{" "}
            <strong>{profile && `${profile.displayName} (${profile.email})`}</strong>.
          </Typography>
        </Box>
        {children}
        <Button color="primary" onClick={signOut} variant="contained">
          Sign out
        </Button>
      </Box>
    </Box>
  );
};

export default TestBanner;
