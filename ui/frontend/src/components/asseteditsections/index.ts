export { default as GeneralInformationEdit } from "./GeneralInformationEdit";
export { default as PersonalDataEdit } from "./PersonalDataEdit";
export { default as RisksEdit } from "./RisksEdit";
export { default as StorageAndSecurityEdit } from "./StorageAndSecurityEdit";
