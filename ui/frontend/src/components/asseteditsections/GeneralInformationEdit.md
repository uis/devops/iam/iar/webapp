### Examples

```jsx
const asset = {
  name: "Asset Name Here",
  department: "bot",
  purpose: "staff_administration",
  private: true,
};

const insts = [
  { value: "bot", label: "St Botolphs College" },
  { value: "dtp", label: "Dept of Theoretical Physics" },
];

<GeneralInformationEdit asset={asset} institutions={insts} />;
```
