### Examples

```jsx
import { LookupProvider } from "../api";

<LookupProvider>
  <ul>
    <li>
      <InstitutionName instName="St Botolph's College" />
    </li>
    <li>
      <InstitutionName instName={null} />
    </li>
    <li>
      <InstitutionName instName="Department of Theoretical Physics" />
    </li>
  </ul>
</LookupProvider>;
```
