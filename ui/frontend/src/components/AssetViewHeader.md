### Examples

```js
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Toolbar from "@material-ui/core/Toolbar";

<BrowserRouter>
  <Switch>
    <Route path="/">
      <Toolbar>
        <AssetViewHeader name="Asset Name Here" />
      </Toolbar>
    </Route>
  </Switch>
</BrowserRouter>;
```
