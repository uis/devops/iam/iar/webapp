### Examples

```js
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Toolbar from "@material-ui/core/Toolbar";
import { AssetHeader, HeaderButton } from "./AssetHeader";

// If supplying an array of buttons, make sure each has a unique key
const buttons = [
  <HeaderButton key="cancel_button">Cancel</HeaderButton>,
  <HeaderButton key="save_button" color="secondary">
    Save Entry
  </HeaderButton>,
];

<BrowserRouter>
  <Switch>
    <Route path="/">
      <Toolbar>
        <AssetHeader action="Action" name="Asset Name" buttons={buttons} />
      </Toolbar>
    </Route>
  </Switch>
</BrowserRouter>;
```
