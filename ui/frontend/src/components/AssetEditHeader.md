### Examples

```js
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Toolbar from "@material-ui/core/Toolbar";
import { Checkbox } from "@material-ui/core";

const [isCreate, setIsCreate] = React.useState(false);

<BrowserRouter>
  <Switch>
    <Route path="/">
      <Checkbox
        checked={isCreate}
        value="create"
        onChange={() => {
          setIsCreate(!isCreate);
        }}
      />
      Create
      <Toolbar>
        <AssetEditHeader
          name="Asset Name Here"
          isCreate={isCreate}
          onSave={() => {
            alert("Save");
          }}
          onDelete={() => {
            alert("Delete");
          }}
        />
      </Toolbar>
    </Route>
  </Switch>
</BrowserRouter>;
```
