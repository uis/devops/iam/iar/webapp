### Examples

```jsx
import { Button } from "@material-ui/core";
const [open, setOpen] = React.useState(false);

const onClose = () => {
  setOpen(false);
};
const onOpen = () => {
  setOpen(true);
};
const onDelete = () => {
  alert("Delete");
  onClose();
};

<>
  <Button onClick={onOpen} variant="contained">
    Open
  </Button>
  <DeleteConfirmation
    open={open}
    onClose={onClose}
    onDelete={onDelete}
    assetName="Name of asset to delete"
  />
</>;
```
