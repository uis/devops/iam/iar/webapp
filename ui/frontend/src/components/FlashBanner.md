### Examples

```jsx
<>
  <FlashBanner>Default flash message</FlashBanner>
  <FlashBanner color="primary">Primary flash message</FlashBanner>
  <FlashBanner color="secondary">Secondary flash message</FlashBanner>
  <FlashBanner color="error">Error flash message</FlashBanner>
</>
```
