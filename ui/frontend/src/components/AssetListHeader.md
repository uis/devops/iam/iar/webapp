### Examples

```js
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Toolbar from "@material-ui/core/Toolbar";

<BrowserRouter>
  <Switch>
    <Route path="/">
      <Toolbar>
        <AssetListHeader title="Assets: St Botolph's College" />
      </Toolbar>
    </Route>
  </Switch>
</BrowserRouter>;
```
