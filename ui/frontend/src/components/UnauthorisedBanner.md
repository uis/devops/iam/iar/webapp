### Examples

```jsx
<UnauthorisedBanner
  profile={{ displayName: "Unauthorised User", email: "bad@example.com" }}
  onSignOut={() => alert("sign out")}
/>
```
