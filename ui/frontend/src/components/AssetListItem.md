### Examples

```jsx
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Table, TableBody } from "@material-ui/core";

const asset = {
  id: "asset-id-here",
  name: "An example asset name",
  isComplete: false,
  department: "DEPTX",
  isPrivate: true,
  updatedAt: "2019-12-25T13:42:21.112233Z",
  allowedMethods: ["GET", "PUT", "DELETE"],
};

const onDelete = (id) => {
  alert(`Delete: ${id}`);
};

<BrowserRouter>
  <Switch>
    <Route path="/">
      <Table>
        <TableBody>
          <AssetListItem asset={asset} onDelete={onDelete} />
        </TableBody>
      </Table>
    </Route>
  </Switch>
</BrowserRouter>;
```
