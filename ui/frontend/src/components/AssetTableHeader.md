### Examples

```jsx
import { Button, Table } from "@material-ui/core";
const [isLoading, setIsLoading] = React.useState(true);
const [ordering, setOrdering] = React.useState(undefined);

<>
  <div>Ordering: {ordering}</div>
  <Button
    variant="contained"
    onClick={() => {
      setIsLoading(!isLoading);
    }}
  >
    {isLoading ? "Stop Loading" : "Start Loading"}
  </Button>
  <Table size="small">
    <AssetTableHeader
      isLoading={isLoading}
      onSetOrdering={setOrdering}
      ordering={ordering}
    />
  </Table>
</>;
```
