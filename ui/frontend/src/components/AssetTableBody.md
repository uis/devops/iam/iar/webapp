### Examples

```jsx
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Button, Table } from "@material-ui/core";
const [isLoading, setIsLoading] = React.useState(false);
const [summaries, setSummaries] = React.useState([]);

const loadSummaries = () => {
  setIsLoading(true);
  setSummaries([
    {
      id: "1",
      name: "First Asset Name",
      isComplete: true,
      department: "InstId1",
      isPrivate: true,
      updatedAt: "2018-01-26T15:08:42.457427Z",
      allowedMethods: ["GET"],
    },
    {
      id: "2",
      name: "Another Asset Name",
      isComplete: false,
      department: "InstId2",
      isPrivate: false,
      updatedAt: "2020-02-11T16:02:22.354422Z",
      allowedMethods: ["GET", "PUT"],
    },
    {
      id: "3",
      name: "Yet Another Asset Name",
      isComplete: false,
      department: "InstId2",
      isPrivate: true,
      updatedAt: "2019-10-01T13:42:21.112233Z",
      allowedMethods: ["GET", "PUT", "DELETE"],
    },
  ]);
  setIsLoading(false);
};

const onDelete = (id) => {
  alert(`Delete: ${id}`);
};
<BrowserRouter>
  <Switch>
    <Route path="/">
      <div>
        {!summaries.length && (
          <Button variant="contained" onClick={loadSummaries}>
            Load Summaries
          </Button>
        )}
        <Table size="small">
          <AssetTableBody
            isLoading={isLoading}
            summaries={summaries}
            onDelete={onDelete}
          />
        </Table>
      </div>
    </Route>
  </Switch>
</BrowserRouter>;
```
