### Examples

```js
<AppBar position="static" />
```

```js
<AppBar position="static">
  <div>
    this is some <em>custom</em> content
  </div>
</AppBar>
```

```js
<AppBar
  position="static"
  onMenuClick={() => {
    alert("menu clicked");
  }}
>
  <div>this shows a menu when shrunk</div>
</AppBar>
```
