/**
 * When API calls fail, the related Promise is reject()-ed with an object implementing this
 * interface.
 */
export interface APIError {
  /** A descriptive error. */
  error: Error;
  /** The response from the API, if any. */
  response?: Response;
  /** The decoded JSON response, if available */
  body?: any;
}

/**
 * Sign in settings
 */
export interface SignInSettings {
  googleClientId: string;
  googleScopes: string[];
}

/**
 * Token exchange/refresh response
 */
export interface Tokens {
  access: string;
  refresh?: string;
}

/**
 * User profile resource
 */
export interface Profile {
  /** Is the user authenticated? */
  isAnonymous: boolean;
  /** For authenticated users, an email address. */
  email?: string;
  /** For authenticated users, some display name. */
  displayName?: string;
  /** For authenticated users, their Lookup groups. */
  groups?: string[];
  /** For authenticated users, their Institutions. */
  institutions?: string[];
  /** For authenticated users, is authorised to use IAR */
  isAuthorised?: boolean;
}

/** A generic list of resources returned from a resource list endpoint. */
export interface IResourceListResponse<Resource> {
  results: Resource[];
  next?: string | null;
  previous?: string | null;
}

/** An assets list response. */
export type LookupInstsResponse = IResourceListResponse<Institution>;

/* Lookup Institution */
export interface Institution {
  instid: string;
  name: string;
}

/** An assets list response. */
export type AssetsResponse = IResourceListResponse<Asset>;

/**
 * Individual Asset
 **/
export interface Asset {
  url: string;
  id: string;
  dataSubject: string[] | null;
  dataCategory: string[] | null;
  riskType: string[] | null;
  storageFormat: string[] | null;
  paperStorageSecurity: string[] | null;
  digitalStorageSecurity: string[] | null;
  isComplete: boolean;
  allowedMethods: string[] | null;
  name: string | null;
  department: string | null;
  purpose: string | null;
  purposeOther: string | null;
  owner: string | null;
  private: boolean | null;
  personalData: boolean | null;
  recipientsOutsideUni: string | null;
  recipientsOutsideEea: string | null;
  recipientsOutsideUniDescription: string | null;
  recipientsOutsideEeaDescription: string | null;
  retention: string | null;
  riskTypeAdditional: string | null;
  storageLocation: string | null;
  createdAt: string;
  updatedAt: string;
  instName?: string | null; // added to asset after load via lookup API, if needed
}

/** Query options for retrieving a list of assets */
export interface AssetsQuery {
  // field to sort by
  ordering?: string;
  // department (instid) to filter by
  department?: string;
}

/** People lookup response */
export type LookupPeopleResponse = IResourceListResponse<Person>;

/** Individual person */
export interface Person {
  id: string;
  name: string;
}
