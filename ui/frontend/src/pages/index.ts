export { default as GenericPage } from "./GenericPage";
export { default as AssetListPage } from "./AssetListPage";
export { default as AssetViewPage } from "./AssetViewPage";
export { default as AssetFormPage } from "./AssetFormPage";
export { default as StaticPage } from "./StaticPage";
export { default as RedirectToMyDeptAssets } from "./RedirectToMyDeptAssets";
