"""
URL routing schema for UI

"""

from django.urls import path
from django.views.generic import TemplateView

app_name = 'ui'

ui_paths = [
    ('assets/<filter>', 'assets-filter'),
    ('assets/', 'assets-list'),
    ('asset/<pk>/edit', 'asset-edit'),
    ('asset/<pk>', 'asset-detail'),
    ('asset/', 'asset-create'),
    ('help/', 'help'),
    ('feedback/', 'feedback'),
    ('', 'home'),
]

urlpatterns = [
    path(p, TemplateView.as_view(template_name="index.html"), name=n)
    for p, n in ui_paths
]
